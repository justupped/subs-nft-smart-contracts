General info: Before we can mint any NFT we need to have few things in place: 
a) Admin account, within emulator it is the contract owner `emulator-account`;
b) user account that was granter Creator role by Admin;
c) regular user account that will receive the NFT;

1. Make sure you have flow-cli installed (ver > 0.17)

2. Run the emulator: `make emulator` and tun deploy `make deploy`

3. Create moderator account: `make create_moderator`

4. Setup moderator account: `make setup_moderator`

5. Create Creator account (the one who will get the NFT): `make create_creator` note the transaction ID and get the result with `make tx id=<id_here>`, you should get an address. Update the address inside `flow.json` under `accounts.creator.address`;

6. Setup account (this creates the SubsArt collection to deposit NFT): `make setup_account_creator`

7. Setup creator `make setup_creator userID=1 addr=<paste from step 3>`

8. Create some art: `make create_art`. Note the transaction Tx and get the result with `make tx id=<id_here>`. Note `artID`;

9. Create user account (the one who will get the NFT): `make create_account` note the transaction ID and get the result with `make tx id=<id_here>`, you should get an address. Update the address inside `flow.json` under `accounts.user.address`;

10. Setup account (this creates the SubsArt collection to deposit NFT): `make setup_account_user`

11. Mint the NFT: `make mint_nft artID=1 addr=<user_address>` user_address here is the one you get from step 6, in a format with 0x at the start. note the transaction ID and get the result with `make tx id=<id_here>`. Note the `nftID`

12. Send the NFT back from user to creator (testing transaction) `make send_nft nftID=<nftID from step 8> addr=<address from step 3>`

The transaction from step 8 should mind and deposit NFT into account of the user.