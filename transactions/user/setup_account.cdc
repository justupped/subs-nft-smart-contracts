import SubsArt from _SUBCONTRACT_

// This transaction sets up an account to use Subs Art
// by storing an empty Art collection and creating
// a public capability for it

transaction {
    let acct: AuthAccount

    prepare(acct: AuthAccount) {
        self.acct = acct
    }

    execute {
        if self.acct.borrow<&SubsArt.Collection>(from: SubsArt.CollectionStoragePath) == nil {
          // create a new Subs Collection
          let collection <- SubsArt.createEmptyCollection() as! @SubsArt.Collection

          // Put the new Collection in storage
          self.acct.save(<-collection, to: SubsArt.CollectionStoragePath)

          // create a public capability for the collection
          self.acct.link<&{SubsArt.ArtCollectionPublic}>(SubsArt.CollectionPublicPath, target: SubsArt.CollectionStoragePath)
        }
        if self.acct.borrow<&SubsArt.SubsUser>(from: SubsArt.SubsUserStoragePath) == nil {
          // create a new User resource
          let user <- SubsArt.createSubsUser() as! @SubsArt.SubsUser

          // Put the new User object into the storage
          self.acct.save(<-user, to: SubsArt.SubsUserStoragePath)

          // create a public capability for the user resource
          self.acct.link<&{SubsArt.SubsUserPublic}>(SubsArt.SubsUserPublicPath, target: SubsArt.SubsUserStoragePath)
        }
    }
}