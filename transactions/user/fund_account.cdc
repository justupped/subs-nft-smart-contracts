import SubsArt from _SUBCONTRACT_
import FungibleToken from _FUNGIBLETOKEN_ /* substituted with a value from https://github.com/onflow/flow-ft#how-to-use-the-fungible-token-contract */
import FlowToken from _FLOWTOKEN_ /* substituted with a value from https://docs.onflow.org/core-contracts/flow-token/ */

// This transaction checks if account has enough storage for some more tokens
// and if not then it transfers funds from service account.
transaction(recipient: Address, requiredFreeSpace: UInt64) {
    let signer: AuthAccount
    let acct: PublicAccount

    prepare(signer: AuthAccount) {
        self.signer = signer
        self.acct = getAccount(recipient)

        logPre(acct: self.acct, requiredFreeSpace: requiredFreeSpace)
    }

    pre {
        storageFree(acct: self.acct) < requiredFreeSpace: "Account funding not required!"
    }

    execute {
        fundAccount(signer: self.signer, recipient: recipient)

        logPost(acct: self.acct)
    }
}

pub fun storageFree(acct: PublicAccount): UInt64 {
    return acct.storageCapacity - acct.storageUsed
}

pub fun fundAccount(signer: AuthAccount, recipient: Address) {
    let amount = 0.001

    let vaultRef = signer.borrow<&FlowToken.Vault>(from: /storage/flowTokenVault) ?? panic("failed to borrow reference to sender vault")
    let sentVault <- vaultRef.withdraw(amount: amount)

    let receiverRef = getAccount(recipient).getCapability(/public/flowTokenReceiver).borrow<&{FungibleToken.Receiver}>() ?? panic("failed to borrow reference to recipient vault")
    receiverRef.deposit(from: <-sentVault)

    log("Added ".concat(amount.toString()).concat(" funds to account '").concat(recipient.toString()).concat("'"))
}

pub fun logPre(acct: PublicAccount, requiredFreeSpace: UInt64) {
     log("Checking if account '".concat(
        acct.address.toString()).concat(
        "' with balance ").concat(
        acct.availableBalance.toString()).concat(
        "/").concat(
        acct.balance.toString()).concat(
        " has at least free space: ").concat(
        requiredFreeSpace.toString()).concat(
        " - storage: ").concat(
        acct.storageUsed.toString()).concat(
        "/").concat(
        acct.storageCapacity.toString()).concat(
        " - free: ").concat(
        storageFree(acct: acct).toString())
    )
}

pub fun logPost(acct: PublicAccount) {
     log("Funds added to account '".concat(
        acct.address.toString()).concat(
        "' with balance ").concat(
        acct.availableBalance.toString()).concat(
        "/").concat(
        acct.balance.toString()).concat(
        " - storage: ").concat(
        acct.storageUsed.toString()).concat(
        "/").concat(
        acct.storageCapacity.toString()).concat(
        " - free: ").concat(
        storageFree(acct: acct).toString())
    )
}