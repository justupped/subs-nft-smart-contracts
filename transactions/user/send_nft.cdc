import SubsArt from _SUBCONTRACT_

// This transaction is what an admin would use to mint a single new NFT
// and deposit it in a user's collection

// Parameters:
//
// nftID: the ID of an NFT which we want to send
// recipientAddr: the Flow address of the account receiving the newly minted NFT

transaction(nftID: UInt64, recipientAddr: Address) {
    // local variable for the creator reference
    let senderRef: &SubsArt.Collection

    prepare(acct: AuthAccount) {
        // borrow a reference to the ArtCollection resource in storage
        self.senderRef = acct.borrow<&SubsArt.Collection>(from: SubsArt.CollectionStoragePath)
          ?? panic("Could not borrow a reference to the sender Collection resource")
    }

    execute {
        // Borrow a reference to the specified nft
        let nft <- self.senderRef.withdraw(withdrawID: nftID)

        // get the public account object for the recipient
        let recipient = getAccount(recipientAddr)

        // get the Collection reference for the receiver
        let receiverRef = recipient.getCapability(SubsArt.CollectionPublicPath).borrow<&{SubsArt.ArtCollectionPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's art collection")

        // deposit the NFT in the receivers collection
        receiverRef.deposit(token: <-nft)
    }
}
