import SubsArt from _SUBCONTRACT_

// This transaction is what an admin would use to mint a single new NFT
// and deposit it in a user's collection. It is a multi-sig transaciton, 
// so that we can create collection inside user's wallet if needed

// Parameters:
//
// nftID: the ID of an NFT which we want to send

transaction(nftID: UInt64) {
    // local variable for the creator reference
    let senderRef: &SubsArt.Collection
    let recipient: AuthAccount

    prepare(acct: AuthAccount, recipient: AuthAccount) {
        self.recipient = recipient
        // borrow a reference to the ArtCollection resource in storage
        self.senderRef = acct.borrow<&SubsArt.Collection>(from: SubsArt.CollectionStoragePath)
          ?? panic("Could not borrow a reference to the sender Collection resource")

          // First, check to see if a Art collection already exists
        if recipient.borrow<&SubsArt.Collection>(from: SubsArt.CollectionStoragePath) == nil {

            // create a new SubsArt Collection
            let collection <- SubsArt.createEmptyCollection() as! @SubsArt.Collection

            // Put the new Collection in storage
            recipient.save(<-collection, to: SubsArt.CollectionStoragePath)

            // create a public capability for the collection
            recipient.link<&{SubsArt.ArtCollectionPublic}>(SubsArt.CollectionPublicPath, target: SubsArt.CollectionStoragePath)
        }
    }

    execute {
        // Borrow a reference to the specified nft
        let nft <- self.senderRef.withdraw(withdrawID: nftID)

        // get the Collection reference for the receiver
        let receiverRef = self.recipient.getCapability(SubsArt.CollectionPublicPath).borrow<&{SubsArt.ArtCollectionPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's Art collection")

        // deposit the NFT in the receivers collection
        receiverRef.deposit(token: <-nft)
    }
}
