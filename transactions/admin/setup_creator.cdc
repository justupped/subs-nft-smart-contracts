import SubsArt from _SUBCONTRACT_

// This transaction sets up an account to use Subs Art
// by storing an empty collection and creating
// a public capability for it

transaction(userID: UInt64, recipientAddr: Address) {
    // Local variable for the Subs Moderator object
    let adminRef: &SubsArt.Moderator
    let userRef: &AnyResource{SubsArt.SubsUserPublic}

    prepare(acct: AuthAccount) {
        // get the public account object for the recipient
        let recipient = getAccount(recipientAddr)

        // borrow a reference to the Moderator resource in storage
        self.adminRef = acct.borrow<&SubsArt.Moderator>(from: SubsArt.ModeratorStoragePath)
            ?? panic("Could not borrow a reference to the Moderator resource")

        // get the Collection reference for the receiver
        self.userRef = recipient.getCapability(SubsArt.SubsUserPublicPath).borrow<&{SubsArt.SubsUserPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's Art collection")
    }

    execute {
        let creator <- self.adminRef.createNewCreator(userID: userID) as! @SubsArt.Creator

        self.userRef.assignCreator(creator: <- creator)

        // Create the gallery in case if it doesn't exist
        if !SubsArt.isGalleryExists(userID: userID) {
            // Create a set with the specified name
            self.adminRef.createGallery(userID: userID)
        }
    }
}