import SubsArt from _SUBCONTRACT_

// This transaction is for the admin to create a new Art and add it
// to the user's Gallery. A gallery is created alongside if didn't exist

// Parameters:
//
// userID: ID of a user we create an Art for
// metadata: a dictionary of all metadata for this Art

transaction(userID: UInt64, metadata: {String: String}) {
    
    // Local variable for the Subs Admin object
    let adminRef: &SubsArt.Admin
    let currGalleryID: UInt32
    let currArtID: UInt32

    prepare(acct: AuthAccount) {
        // borrow a reference to the Admin resource in storage
        self.adminRef = acct.borrow<&SubsArt.Admin>(from: SubsArt.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
        self.currGalleryID = SubsArt.nextGalleryID;
        self.currArtID = SubsArt.nextArtID;
    }

    execute {
        // Create the gallery in case if it doesn't exist
        if !SubsArt.isGalleryExists(userID: userID) {
            // Create a set with the specified name
            self.adminRef.createGallery(userID: userID)
        }
         // Create an art with the specified metadata
        self.adminRef.createArt(metadata: metadata)

        // Borrow a reference to the set to be added to
        let setRef = self.adminRef.borrowGallery(userID: userID)

        // Add the specified play ID
        setRef.addArt(artID: self.currArtID)
    }

    post {
        SubsArt.getArtMetaData(artID: self.currArtID) != nil:
            "artID does not exist"
        SubsArt.getArtsInGallery(userID: userID)!.contains(self.currArtID): 
            "set does not contain artID"
    }
}