import SubsArt from _SUBCONTRACT_

// This transaction is what an admin would use to mint a single new NFT
// and deposit it in a user's collection

// Parameters:
//
// galleryUserID: the User ID of an gallery owner containing the target art
// artID: the ID of an art from which a new NFT is minted
// recipientAddr: the Flow address of the account receiving the newly minted NFT

transaction(galleryUserID: UInt64, artID: UInt32, recipientAddr: Address) {
    // local variable for the admin reference
    let adminRef: &SubsArt.Admin

    prepare(acct: AuthAccount) {
        // borrow a reference to the Admin resource in storage
        self.adminRef = acct.borrow<&SubsArt.Admin>(from: SubsArt.AdminStoragePath)!
    }

    execute {
        // Borrow a reference to the specified set
        let setRef = self.adminRef.borrowGallery(userID: galleryUserID)

        // Mint a new NFT
        let nft <- setRef.mintNFT(artID: artID)

        // get the public account object for the recipient
        let recipient = getAccount(recipientAddr)

        // get the Collection reference for the receiver
        let receiverRef = recipient.getCapability(SubsArt.CollectionPublicPath).borrow<&{SubsArt.ArtCollectionPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's Art collection")

        // deposit the NFT in the receivers collection
        receiverRef.deposit(token: <-nft)
    }
}
