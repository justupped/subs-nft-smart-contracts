import SubsArt from _SUBCONTRACT_

transaction() {
    // Local variable for the Subs Admin object
    let adminRef: &SubsArt.Admin
    let moderatorRef: AuthAccount

    prepare(acct: AuthAccount, moderatorAcct: AuthAccount) {
        // borrow a reference to the Admin resource in storage
        self.adminRef = acct.borrow<&SubsArt.Admin>(from: SubsArt.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
        self.moderatorRef = moderatorAcct
    }

    execute {
        let moderator <- self.adminRef.createNewModerator() as! @SubsArt.Moderator
        self.moderatorRef.save<@SubsArt.Moderator>(<- moderator, to: SubsArt.ModeratorStoragePath)
    }
}