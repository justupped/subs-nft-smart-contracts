import SubsArt from _SUBCONTRACT_

// This transaction locks up the gallery that was assigned to the specifc user
transaction(userID: UInt64) {
    // Local variable for the Subs Admin object
    let adminRef: &SubsArt.Admin

    prepare(acct: AuthAccount) {
        // borrow a reference to the Admin resource in storage
        self.adminRef = acct.borrow<&SubsArt.Admin>(from: SubsArt.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
    }

    execute {
        self.adminRef.lockGallery(userID: userID)
    }

    post {
        self.adminRef.borrowGallery(userID: userID).locked == true:
            "gallery has not been locked properly"
    }
}