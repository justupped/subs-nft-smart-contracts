import SubsArt from _SUBCONTRACT_

// This transaction is for the Creator to create a new Art and add it
// to the own's user's Gallery. A gallery is created alongside if didn't exist

// Parameters:
//
// supply: a number of items we want to mit
// metadata: a dictionary of all metadata for this Art

transaction(supply: UInt64, metadata: {String: String}) {
    
    // Local variable for the creator object
    let currArtID: UInt32
    let galleryRef: &SubsArt.Gallery{SubsArt.GalleryCreator}
    let receiverRef: &AnyResource{SubsArt.ArtCollectionPublic}

    prepare(acct: AuthAccount) {
        if (supply < UInt64(1)) {
            panic("Supply can not be less than 1")
        }
        // borrow a reference to the Subs User resource in storage
        let userRef = acct.borrow<&SubsArt.SubsUser>(from: SubsArt.SubsUserStoragePath)
            ?? panic("Could not borrow a reference to the SubsUser resource")
        self.currArtID = SubsArt.nextArtID;
        self.galleryRef = userRef.borrowCreatorGallery()
            ?? panic("Could not borrow a reference to the creator Gallery resource")

        // get the Collection reference for the receiver
        self.receiverRef = acct.getCapability(SubsArt.CollectionPublicPath).borrow<&{SubsArt.ArtCollectionPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's Art collection")
    }

    execute {
        // Create an art with the specified metadata
        let artID = self.galleryRef.createArt(metadata: metadata)

        // Mint a new NFT
        let tokens <- self.galleryRef.batchMintNFT(artID: artID, quantity: supply)

        // deposit the NFT in the receivers collection
        self.receiverRef.batchDeposit(tokens: <- tokens)
    }

    post {
        SubsArt.getArtMetaData(artID: self.currArtID) != nil:
            "artID does not exist"
    }
}