import SubsArt from _SUBCONTRACT_

// This transaction is for the Creator to create a new Art and add it
// to the user's Gallery. A gallery is created alongside if didn't exist

// Parameters:
//
// metadata: a dictionary of all metadata for this Art

transaction(metadata: {String: String}) {
    
    // Local variable for the creator object
    let currArtID: UInt32
    let galleryRef: &SubsArt.Gallery{SubsArt.GalleryCreator}

    prepare(acct: AuthAccount) {
        // borrow a reference to the Subs User resource in storage
        let userRef = acct.borrow<&SubsArt.SubsUser>(from: SubsArt.SubsUserStoragePath)
            ?? panic("Could not borrow a reference to the SubsUser resource")
        self.currArtID = SubsArt.nextArtID;
        self.galleryRef = userRef.borrowCreatorGallery()
            ?? panic("Could not borrow a reference to the creator Gallery resource")
    }

    execute {
        // Create an art with the specified metadata
        self.galleryRef.createArt(metadata: metadata)
    }

    post {
        SubsArt.getArtMetaData(artID: self.currArtID) != nil:
            "artID does not exist"
    }
}