import SubsArt from _SUBCONTRACT_

// This transaction is what an Creator would use to mint a single new NFT
// and deposit it in a user's collection. It is a multi-sig transaciton, 
// so that we can create collection inside user's wallet if needed

// Parameters:
//
// artID: the ID of an art from which a new NFT is minted

transaction(artID: UInt32) {
    // local variable for the creator's Gallery reference
    let galleryRef: &SubsArt.Gallery{SubsArt.GalleryCreator}
    let recipient: AuthAccount

    prepare(acct: AuthAccount, recipient: AuthAccount) {
        self.recipient = recipient
        // borrow a reference to the Subs User resource in storage
        let userRef = acct.borrow<&SubsArt.SubsUser>(from: SubsArt.SubsUserStoragePath)
            ?? panic("Could not borrow a reference to the SubsUser resource")

        self.galleryRef = userRef.borrowCreatorGallery()
            ?? panic("Could not borrow a reference to the creator Gallery resource")

        // First, check to see if a NFT collection already exists
        if recipient.borrow<&SubsArt.Collection>(from: SubsArt.CollectionStoragePath) == nil {

            // create a new SubsArt Collection
            let collection <- SubsArt.createEmptyCollection() as! @SubsArt.Collection

            // Put the new Collection in storage
            recipient.save(<-collection, to: SubsArt.CollectionStoragePath)

            // create a public capability for the collection
            recipient.link<&{SubsArt.ArtCollectionPublic}>(SubsArt.CollectionPublicPath, target: SubsArt.CollectionStoragePath)
        }
    }

    execute {
        // Mint a new NFT
        let nft <- self.galleryRef.mintNFT(artID: artID)

        // get the Collection reference for the receiver
        let receiverRef = self.recipient.getCapability(SubsArt.CollectionPublicPath).borrow<&{SubsArt.ArtCollectionPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's art collection")

        // deposit the NFT in the receivers collection
        receiverRef.deposit(token: <-nft)
    }
}
