import SubsArt from _SUBCONTRACT_

// This transaction is what a Creator would use to mint a single new NFT
// and deposit it in a user's collection

// Parameters:
//
// artID: the ID of an art from which a new NFT is minted
// recipientAddr: the Flow address of the account receiving the newly minted NFT

transaction(artID: UInt32, recipientAddr: Address) {
    // local variable for the creator's Gallery reference
    let galleryRef: &SubsArt.Gallery{SubsArt.GalleryCreator}

    prepare(acct: AuthAccount) {
        // borrow a reference to the Subs User resource in storage
        let userRef = acct.borrow<&SubsArt.SubsUser>(from: SubsArt.SubsUserStoragePath)
            ?? panic("Could not borrow a reference to the SubsUser resource")
        // Borrow a reference to the set to be added to
        self.galleryRef = userRef.borrowCreatorGallery()
            ?? panic("Could not borrow a reference to the creator Gallery resource")
    }

    execute {
        // Mint a new NFT
        let nft <- self.galleryRef.mintNFT(artID: artID)

        // get the public account object for the recipient
        let recipient = getAccount(recipientAddr)

        // get the Collection reference for the receiver
        let receiverRef = recipient.getCapability(SubsArt.CollectionPublicPath).borrow<&{SubsArt.ArtCollectionPublic}>()
            ?? panic("Cannot borrow a reference to the recipient's Art collection")

        // deposit the NFT in the receivers collection
        receiverRef.deposit(token: <-nft)
    }
}
