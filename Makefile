create_creator:
	@flow accounts create --key 6aec3f2c0ced714dbe415b9b405986133cce7ab9a522c70657eacf972b91b656a3eaca499571e396b6be701912e129b6ef2346e0567327443215ea9cea39ffda --signer emulator-account

create_moderator:
	@flow accounts create --key ef0289ff51c628469499e6890e15bc2af32aadf47926677b3acfa2fb454ebbaec799090cfee030089bc4befe740675e8849f0ebbbce4876b325a4ca6b0327ddf --key-weight 1000 --signer emulator-account

setup_moderator: export TRANSACTION=transactions/admin/setup_moderator.cdc
setup_moderator:
	@flow transactions build $(TRANSACTION) \
  --authorizer emulator-account \
  --authorizer moderator \
  --proposer emulator-account \
  --payer emulator-account \
  --filter payload --save ./built.rlp

	@flow transactions sign ./built.rlp --signer moderator \
  --filter payload --save signed.rlp -y

	@flow transactions sign ./signed.rlp --signer emulator-account \
  --filter payload --save signed2.rlp -y

	@flow transactions send-signed ./signed2.rlp

	@rm built.rlp signed.rlp signed2.rlp

setup_moderator_local: FLOW_CONTRACTS_SUBACCOUNT_ADDRESS=f8d6e0586b0a20c7
setup_moderator_local:
	@sed 's/_SUBCONTRACT_/0x$(FLOW_CONTRACTS_SUBACCOUNT_ADDRESS)/g' transactions/admin/setup_moderator.cdc > transactions/admin/setup_moderator_local.cdc
	@$(MAKE) setup_moderator TRANSACTION=transactions/admin/setup_moderator_local.cdc

setup_creator:
	@flow transactions send ./transactions/admin/setup_creator.cdc --args-json="[{\"type\": \"UInt64\" , \"value\": \"$(userID)\" }, {\"type\": \"Address\" , \"value\": \"$(addr)\" }]" --signer moderator

setup_user_testnet:
	@flow transactions send ./transactions/user/setup_account.cdc --network testnet --signer testnet-account2

setup_creator_testnet:
	@flow transactions send ./transactions/admin/setup_creator.cdc --args-json="[{\"type\": \"UInt64\" , \"value\": \"$(userID)\" }, {\"type\": \"Address\" , \"value\": \"$(addr)\" }]" --network testnet --signer testnet-account

create_art:
	@flow transactions send ./transactions/creator/create_art.cdc --args-json="[{\"type\": \"Dictionary\" , \"value\": [ { \"key\": {\"type\": \"String\" , \"value\": \"key\" }, \"value\": {\"type\": \"String\" , \"value\": \"value\" }}]}]" --signer creator

create_account:
	@flow accounts create --key 6f87c1cc481fb2a30bc858202d124853f16ea86f8d5a009756fa405630b180d69483f31053f9e12eb432bd672813879b5cce1c4ed2cae802b51cce495909ed41 --signer emulator-account

setup_account_creator:
	@flow transactions send ./transactions/user/setup_account.cdc --signer creator

setup_account_user:
	@flow transactions send ./transactions/user/setup_account.cdc --signer user

mint_nft:
	@flow transactions send ./transactions/creator/mint_nft.cdc --args-json="[{\"type\": \"UInt32\" , \"value\": \"$(artID)\" }, {\"type\": \"Address\" , \"value\": \"$(addr)\" }]" --signer creator

mint_self_nft:
	@flow transactions send ./transactions/creator/create_art_self_mint.cdc --args-json="[{\"type\": \"UInt64\" , \"value\": \"$(supply)\" }, {\"type\": \"Dictionary\" , \"value\": [ { \"key\": {\"type\": \"String\" , \"value\": \"key\" }, \"value\": {\"type\": \"String\" , \"value\": \"value\" }}]}]" --signer creator

send_nft:
	@flow transactions send ./transactions/user/send_nft.cdc --args-json="[{\"type\": \"UInt64\" , \"value\": \"$(nftID)\" }, {\"type\": \"Address\" , \"value\": \"$(addr)\" }]" --signer user

send_nft_msig:
	@flow transactions build ./transactions/user/send_nft_msig.cdc \
  --authorizer creator \
  --authorizer user \
  --proposer creator \
  --payer creator \
  --filter payload --save ./built.rlp
	@flow transactions sign ./built.rlp --signer user \
  --filter payload --save signed.rlp -y
	@flow transactions sign ./signed.rlp --signer creator \
  --filter payload --save signed2.rlp -y
	@flow transactions send-signed ./signed2.rlp

lock_gallery:
	@flow transactions send ./transactions/admin/lock_gallery.cdc --args-json="[{\"type\": \"UInt64\" , \"value\": \"$(userID)\" }]" --signer emulator-account

unlock_gallery:
	@flow transactions send ./transactions/admin/unlock_gallery.cdc --args-json="[{\"type\": \"UInt64\" , \"value\": \"$(userID)\" }]" --signer emulator-account

tx:
	@flow transactions get $(id)

emulator:
	@flow emulator start -f ./flow.json

deploy_utils:
	@flow accounts add-contract NonFungibleToken ./contracts/onflow/NonFungibleToken.cdc

deploy:
	@flow accounts add-contract SubsArt ./contracts/SubsArt.cdc

deploy_local: FLOW_CONTRACTS_SUBACCOUNT_ADDRESS=f8d6e0586b0a20c7
deploy_local:
	@sed 's/0x1d7e57aa55817448/0x$(FLOW_CONTRACTS_SUBACCOUNT_ADDRESS)/g' contracts/SubsArt.cdc > contracts/SubsArt_local.cdc
	@flow project deploy --update

setup_local: deploy_local create_moderator setup_moderator_local